package com.runvido.ui.tutorial

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.runvido.R
import com.runvido.ui.BaseActivity
import com.runvido.ui.scanner.ScannerActivity
import com.runvido.utils.click
import kotlinx.android.synthetic.main.tutorial_activity.*

private const val TUTORIAL_PREFERENCES: String = "tutorial_preferences"
private const val SHOW_TUTORIAL: String = "show_tutorial"

class TutorialActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tutorial_activity)

        val sharedPreferences = getSharedPreferences(TUTORIAL_PREFERENCES, Context.MODE_PRIVATE)
        val showTutorial = sharedPreferences.getBoolean(SHOW_TUTORIAL, true)
        if (!showTutorial) {
            startScannerActivity()
        }

        startButton.click {
            sharedPreferences.edit().putBoolean(SHOW_TUTORIAL, false).apply()
            startScannerActivity()
        }
    }

    private fun startScannerActivity() {
        startActivity(Intent(this, ScannerActivity::class.java))
        finish()
    }
}