package com.runvido.ui.splash

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.github.florent37.runtimepermission.RuntimePermission
import com.runvido.R
import com.runvido.ui.BaseActivity
import com.runvido.ui.tutorial.TutorialActivity

class SplashActivity : BaseActivity() {

    private var handler: Handler = Handler(Looper.getMainLooper())
    private var permissionsAccepted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.spash_activity)
        ensurePermissions()
    }

    private fun ensurePermissions() {
        RuntimePermission.askPermission(this).onAccepted {
            permissionsAccepted = true
            startTutorialActivity()
        }.onDenied { finish() }
            .onForeverDenied { finish() }
            .request(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ).ask()
    }

    override fun onResume() {
        super.onResume()
        if (permissionsAccepted) {
            startTutorialActivity()
        }
    }

    private fun startTutorialActivity() {
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed({
            startActivity(Intent(this, TutorialActivity::class.java))
            finish()
        }, 2000)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacksAndMessages(null)
    }
}