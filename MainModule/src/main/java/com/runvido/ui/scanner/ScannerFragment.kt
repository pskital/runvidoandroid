package com.runvido.ui.scanner

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.MotionEvent.ACTION_UP
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.runvido.R
import com.runvido.exceptions.VideoTargetException
import com.runvido.module.models.VuforiaTarget
import com.runvido.module.utils.StatisticType
import com.runvido.module.vuforia.ApplicationException
import com.runvido.repository.TargetsRepository
import com.runvido.ui.BaseFragment
import com.runvido.utils.click
import com.runvido.utils.compactActivity
import com.runvido.utils.hideKeyboard
import com.runvido.viewmodel.ScannerViewModel
import com.runvido.vuforia.VuforiaController
import com.runvido.vuforia.VuforiaController.VuforiaCallBacks
import com.vuforia.ImageTarget
import kotlinx.android.synthetic.main.scanner_activity.*
import kotlinx.android.synthetic.main.scanner_fragment.*
import kotlinx.android.synthetic.main.view_link_button.*
import kotlinx.android.synthetic.main.view_pin.*
import kotlinx.android.synthetic.main.view_save_energy.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.security.MessageDigest
import java.util.*
import kotlin.text.Charsets.UTF_8


class ScannerFragment : BaseFragment(), VuforiaCallBacks, TargetsRepository.TargetListener {

    private lateinit var vuforiaController: VuforiaController
    private lateinit var scannerViewModel: ScannerViewModel
    private lateinit var gestureDetector: GestureDetector
    private lateinit var scannerAnimation: Animation

    private var passwordTextWatcher: TextWatcher? = null
    private var alertDialog: AlertDialog? = null
    private var snackBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scannerAnimation = AnimationUtils.loadAnimation(activity, R.anim.scanner)
        scannerViewModel = ViewModelProvider(this, viewModelFactory)
            .get(ScannerViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.scanner_fragment, container, false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vuforiaController = VuforiaController(compactActivity, this, view)
        scannerAnimationView.doOnLayout {
            scannerAnimationView?.translationY = -scannerAnimationView.height.toFloat()
        }

        gestureDetector = GestureDetector(requireActivity(), simpleGestureListener)
        touchHandlerView.setOnTouchListener { _, event ->
            gestureDetector.onTouchEvent(event)
            if (event.action == ACTION_UP) {
                //scannerViewModel.sendStatistic("test", 1000.0, null, "DD046CC7-4F9B-47D3-9F99-B3A298EDF7A5")
            }
            true
        }

        pinView.click {
            pinView.visibility = View.GONE
            hideSaveEnergyLayout()
            pin.text = null
            hideKeyboard()
        }

        saveEnergyLayout?.click {
            hideSaveEnergyLayout()
        }
    }

    private fun hideSaveEnergyLayout() {
        if (!vuforiaController.vuforiaInitialized) {
            return
        }

        vuforiaController.startSaveEnergyTimer()
        if (saveEnergyLayout?.visibility == View.VISIBLE) {
            saveEnergyLayout?.visibility = View.GONE
            if (!vuforiaController.vuforiaStarted) {
                vuforiaController.resumeVuforia()
            }
        }
    }

    private var simpleGestureListener = object : GestureDetector.SimpleOnGestureListener() {

        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            vuforiaController.onSingleTapConfirmed(e)
            return true
        }
    }

    override fun onConfigurationChanged(config: Configuration) {
        super.onConfigurationChanged(config)
        vuforiaController.onConfigurationChanged()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        vuforiaController.clearVuforia()
        alertDialog?.dismiss()
    }

    override fun onTargetLock(vuforiaTarget: VuforiaTarget) {
        showPinView(vuforiaTarget)
    }

    override fun openLinkUrl(vuforiaTarget: VuforiaTarget) {
        val link = vuforiaTarget.vuforiaVideo.link
        Timber.d("open link: %s", link)
        try {
            if (link.isNotEmpty()) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(browserIntent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onInitVuforiaError(error: ApplicationException) {
        val builder = AlertDialog.Builder(activity)
        builder.setPositiveButton("Close") { dialog: DialogInterface, _: Int ->
            dialog.dismiss()

            startActivity(Intent(requireActivity(), ScannerActivity::class.java))
            activity?.finish()
        }
        builder.setMessage(error.message)
        builder.setCancelable(false)
        alertDialog = builder.create()
        alertDialog?.show()
    }

    override fun showLinkView(vuforiaTarget: VuforiaTarget) {
        GlobalScope.launch(Dispatchers.Main) {
            linkButton?.text = vuforiaTarget.vuforiaVideo.buttonText
            linkLayout?.visibility = View.VISIBLE
            linkButton?.click {
                vuforiaController.onSendStatistic(StatisticType.BUTTON_CLICK)
                openLinkUrl(vuforiaTarget)
            }
        }
    }

    override fun hideLinkView() {
        GlobalScope.launch(Dispatchers.Main) { linkLayout?.visibility = View.GONE }
    }

    override fun setProgressBarVisibility(visibility: Int) {
        GlobalScope.launch(Dispatchers.Main) { progressBar?.visibility = visibility }
    }

    override fun onTargetRecognized(recognizedTarget: VuforiaTarget) {
        cancelScannerAnimation()
    }

    override fun onPlayVideo() {
        cancelScannerAnimation()
        hideTopView()
    }

    override fun onNotRecognizedTarget() {
        startScannerAnimation()
        showTopView()
    }

    override fun onSaveEnergy() {
        saveEnergyLayout?.visibility = View.VISIBLE
    }

    override fun onVuforiaStarted() {
        scannerViewModel.requestLocation()
        startScannerAnimation()
    }

    override fun onPauseVuforia() {
        cancelScannerAnimation()
    }

    private fun startScannerAnimation() {
        GlobalScope.launch(Dispatchers.Main) {
            scannerAnimationView?.startAnimation(scannerAnimation)
        }
    }

    private fun cancelScannerAnimation() {
        GlobalScope.launch(Dispatchers.Main) {
            scannerAnimationView?.clearAnimation()
        }
    }

    private fun hideTopView() {
        activity?.runOnUiThread {
            activity?.infoButton?.visibility = View.GONE
        }
    }

    private fun showTopView() {
        activity?.runOnUiThread {
            activity?.infoButton?.visibility = View.VISIBLE
        }
    }

    override fun sendStatistic(
        action: String,
        videoTime: Double,
        vuforiaTarget: VuforiaTarget,
        videoSessionId: String
    ) {
        scannerViewModel.sendStatistic(action, videoTime, vuforiaTarget, videoSessionId)
    }

    override fun requestDownloadTarget(recognizedTarget: ImageTarget) {
        cancelScannerAnimation()

        if (snackBar?.isShown == true || pinView?.visibility == View.VISIBLE) {
            return
        }

        scannerViewModel.getTarget(recognizedTarget.uniqueTargetId, this)
        setProgressBarVisibility(View.VISIBLE)
    }

    override fun loadTargetSuccess(vuforiaTarget: VuforiaTarget) {
        vuforiaController.addTarget(vuforiaTarget)
        setProgressBarVisibility(View.GONE)
    }

    override fun loadTargetError(throwable: Throwable) {
        setProgressBarVisibility(View.GONE)
        throwable.printStackTrace()

        val context = this.context
        if (rootView == null || context == null || snackBar?.isShown == true) {
            return
        }

        var message: String? = null
        if (throwable is VideoTargetException) {
            message = throwable.message
        }

        val snackBar = Snackbar.make(
            rootView,
            message ?: getString(R.string.try_again_later),
            Snackbar.LENGTH_LONG
        )
        val snackBarView: View = snackBar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        snackBar.show()

        this.snackBar = snackBar
    }

    private fun showPinView(vuforiaTarget: VuforiaTarget) {
        GlobalScope.launch(Dispatchers.Main) {
            if (pinView?.visibility == View.GONE && vuforiaController.isTargetLocked(vuforiaTarget)) {
                pinView?.visibility = View.VISIBLE

                pin?.removeTextChangedListener(passwordTextWatcher)
                passwordTextWatcher = object : TextWatcher {
                    override fun beforeTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun onTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                        pin.setItemBackgroundResources(R.drawable.bacground_password)
                    }

                    override fun afterTextChanged(editable: Editable) {
                        if (editable.length == 4) {
                            val md5 = md5(editable.toString())
                            if (md5.toUpperCase(Locale.getDefault()) == vuforiaTarget.password) {
                                vuforiaController.unlockTarget(vuforiaTarget)
                                pinView?.visibility = View.GONE
                                hideKeyboard()
                            }
                            pin.text = null
                        }
                    }
                }
                pin.addTextChangedListener(passwordTextWatcher)
            }
        }
    }

    private fun md5(str: String): String =
        MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8)).toHex()

    private fun ByteArray.toHex() = joinToString("") { "%02x".format(it) }
}