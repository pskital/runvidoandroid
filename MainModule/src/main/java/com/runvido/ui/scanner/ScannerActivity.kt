package com.runvido.ui.scanner

import android.Manifest
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import com.github.florent37.runtimepermission.RuntimePermission
import com.runvido.R
import com.runvido.ui.BaseActivity
import com.runvido.utils.click
import com.runvido.utils.hasInternetConnection
import kotlinx.android.synthetic.main.scanner_activity.*


class ScannerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scanner_activity)
        ensurePermissions()
    }

    private fun ensurePermissions() {
        RuntimePermission.askPermission(this).onAccepted {
            startCameraFragment()
        }.onDenied { finish() }
            .onForeverDenied { finish() }
            .request(
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ).ask()

        infoButton.click {
            showInfoDialog()
        }
    }

    override fun onBackPressed() {
        showCloseDialog()
    }

    private fun showCloseDialog() {
        val factory = LayoutInflater.from(this)
        val dialog: View = factory.inflate(
            R.layout.exit_dialog, null
        )
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setView(dialog)
        val dialogButtonOk = dialog.findViewById<View>(R.id.dialog_button_ok) as Button
        dialogButtonOk.setOnClickListener {
            alertDialog.dismiss()
            finish()
        }
        val dialogButtonCancel = dialog.findViewById<View>(R.id.dialog_button_cancel) as Button
        dialogButtonCancel.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    private fun showInfoDialog() {
        val factory = LayoutInflater.from(this)
        val dialog = factory.inflate(
            R.layout.info_dialog, null
        )
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setView(dialog)
        val dialogButtonWebsite =
            dialog.findViewById<View>(R.id.dialog_button_go_to_website) as Button
        val phone = dialog.findViewById<View>(R.id.phone) as ImageView
        val email = dialog.findViewById<View>(R.id.email) as ImageView
        val facebook = dialog.findViewById<View>(R.id.facebook) as ImageView
        val youtube = dialog.findViewById<View>(R.id.youtube) as ImageView
        val instagram = dialog.findViewById<View>(R.id.instagram) as ImageView
        val close = dialog.findViewById<View>(R.id.close) as ImageView
        dialogButtonWebsite.setOnClickListener { v: View? ->
            viewIntent(
                "http://www.runvido.com"
            )
        }
        phone.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                val intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "+48533307110"))
                startActivity(intent)
            }
        }
        email.setOnClickListener {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "office@runvido.com", null
                )
            )
            startActivity(Intent.createChooser(emailIntent, "Wyślij email"))
        }
        facebook.setOnClickListener {
            try {
                packageManager
                    .getPackageInfo("com.facebook.katana", 0)
                viewIntent("fb://facewebmodal/f?href=https://www.facebook.com/runvido")
            } catch (e: Exception) {
                viewIntent("https://www.facebook.com/runvido")
            }
        }
        youtube.setOnClickListener {
            val uri: Uri =
                Uri.parse("https://www.youtube.com/channel/UCKY4BjYUVLXaZwhYRT0qVqA")
            val likeIng: Intent = Intent(Intent.ACTION_VIEW, uri)
            likeIng.setPackage("com.google.android.youtube")
            try {
                packageManager
                    .getPackageInfo("com.google.android.youtube", 0)
                startActivity(Intent.createChooser(likeIng, "Wybierz aplikacje"))
            } catch (e: Exception) {
                startActivity(
                    Intent.createChooser(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://www.youtube.com/channel/UCKY4BjYUVLXaZwhYRT0qVqA")
                        ), "Wybierz aplikacje"
                    )
                )
            }
        }
        instagram.setOnClickListener {
            val uri: Uri = Uri.parse("http://instagram.com/_u/runvido")
            val likeIng = Intent(Intent.ACTION_VIEW, uri)
            likeIng.setPackage("com.instagram.android")
            try {
                startActivity(Intent.createChooser(likeIng, "Wybierz aplikacje"))
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent.createChooser(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://instagram.com/runvido")
                        ), "Wybierz aplikacje"
                    )
                )
            }
        }
        close.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    private fun viewIntent(url: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(Intent.createChooser(i, "Wybierz aplikacje"))
    }

    private fun startCameraFragment() {
        if (hasInternetConnection()) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.contentFragment, ScannerFragment(), "ScannerFragment")
                .commitAllowingStateLoss()
        } else {
            val builder = AlertDialog.Builder(this)
            builder.setPositiveButton("Close") { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
                finish()
            }
            val message = getString(R.string.internet_connection_required)
            builder.setMessage(message)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.show()
        }
    }
}
