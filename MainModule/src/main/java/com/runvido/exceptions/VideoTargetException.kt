package com.runvido.exceptions

open class VideoTargetException(message: String?) : Exception(message)