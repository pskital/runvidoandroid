@file:Suppress("DEPRECATION")

package com.runvido.vuforia

import android.content.pm.ActivityInfo
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.runvido.R
import com.runvido.module.models.VuforiaTarget
import com.runvido.module.utils.StatisticType
import com.runvido.module.utils.VuforiaConfig
import com.runvido.module.vuforia.*
import com.runvido.module.vuforia.VideoPlayerHelper.VideoListener
import com.runvido.viewmodel.ScannerViewModel
import com.vuforia.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class VuforiaController(
    private val context: FragmentActivity,
    private val vuforiaCallBacks: VuforiaCallBacks,
    private val view: View
) : ApplicationControl, VideoListener, LifecycleObserver,
    VideoPlaybackRenderer.TargetRendererListener {

    interface VuforiaCallBacks {

        fun sendStatistic(
            action: String,
            videoTime: Double,
            vuforiaTarget: VuforiaTarget,
            videoSessionId: String
        )

        fun requestDownloadTarget(recognizedTarget: ImageTarget)

        fun onTargetRecognized(recognizedTarget: VuforiaTarget)

        fun onInitVuforiaError(error: ApplicationException)

        fun showLinkView(vuforiaTarget: VuforiaTarget)

        fun onTargetLock(vuforiaTarget: VuforiaTarget)

        fun openLinkUrl(vuforiaTarget: VuforiaTarget)

        fun setProgressBarVisibility(visibility: Int)

        fun onNotRecognizedTarget()

        fun onVuforiaStarted()

        fun onPauseVuforia()

        fun hideLinkView()

        fun onSaveEnergy()

        fun onPlayVideo()
    }

    companion object {
        private const val SAVE_ENERGY_DELAY: Long = 60000
    }

    private val saveEnergyHandler = Handler(Looper.getMainLooper())
    private val playerHelperList: ArrayList<VideoPlayerHelper>
    private val vuforiaAppSession: ApplicationSession

    private var playbackRenderer: VideoPlaybackRenderer? = null
    private var iconTextures = Vector<Texture>()
    private var targetFinder: TargetFinder? = null
    private var glView: ApplicationGLView? = null
    private var firstLaunch: Boolean = false
    private var targetRecognized = false
    private var videoCount: Int = 0

    var vuforiaInitialized: Boolean = false
    var vuforiaStarted: Boolean = false

    init {
        context.lifecycle.addObserver(this)
        videoCount = ScannerViewModel.MAX_VIDEO_COUNT

        vuforiaAppSession = ApplicationSession(this)
        vuforiaAppSession.initAR(context, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

        val assets = context.assets
        iconTextures.add(Texture.loadTextureFromApk("vuforia/ic_launcher.png", assets))
        iconTextures.add(Texture.loadTextureFromApk("vuforia/ic_launcher_empty.png", assets))
        iconTextures.add(Texture.loadTextureFromApk("vuforia/busy.png", assets))
        iconTextures.add(Texture.loadTextureFromApk("vuforia/error.png", assets))
        iconTextures.add(Texture.loadTextureFromApk("vuforia/gallery.png", assets))

        playerHelperList = ArrayList()
        for (i in 0 until videoCount) {
            val videoPlayerHelper = VideoPlayerHelper()
            videoPlayerHelper.init()
            videoPlayerHelper.setListener(this)
            playerHelperList.add(videoPlayerHelper)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        resumeVuforia()
    }

    fun resumeVuforia() {
        Timber.d("ScannerFragment resumeVuforia")
        try {
            vuforiaCallBacks.setProgressBarVisibility(View.VISIBLE)
            vuforiaAppSession.onResume()
            glView?.onResume()
            glView?.visibility = View.VISIBLE
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        clearSaveEnergyDelay()
        pauseVuforia()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        for (i in 0 until videoCount) {
            val videoPlayerHelper = playerHelperList[i]
            if (videoPlayerHelper.fileName == null) {
                break
            }
            videoPlayerHelper.pause()
        }
    }
    
    private fun pauseVuforia() {
        Timber.d("ScannerFragment pauseVuforia")

        try {
            for (i in 0 until videoCount) {
                val videoPlayerHelper = playerHelperList[i]
                if (videoPlayerHelper.fileName == null) {
                    break
                }
                videoPlayerHelper.pause()
            }

            vuforiaCallBacks.onPauseVuforia()
            vuforiaAppSession.pauseAR()
            vuforiaStarted = false

            glView?.visibility = View.INVISIBLE
            glView?.onPause()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun doInitTrackers(): Boolean {
        Timber.d("ScannerFragment doInitTrackers")
        var result = true
        val trackerManager = TrackerManager.getInstance()
        val tracker = trackerManager.initTracker(ObjectTracker.getClassType())
        if (tracker == null) {
            result = false
        }
        return result
    }

    override fun doLoadTrackersData(): Boolean {
        Timber.d("ScannerFragment doLoadTrackersData")
        return try {
            val trackerManager = TrackerManager.getInstance()
            val objectTracker =
                trackerManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker
            val targetFinder = objectTracker.targetFinder
            if (targetFinder.startInit(VuforiaConfig.ACCESS_KEY, VuforiaConfig.SECRET_KEY)) {
                targetFinder.waitUntilInitFinished()
            }
            val resultCode = targetFinder.initState
            if (resultCode != TargetFinder.INIT_SUCCESS) {
                return false
            }
            this.targetFinder = targetFinder
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override fun doStartTrackers(): Boolean {
        Timber.d("ScannerFragment doStartTrackers")
        var result = true
        if (targetFinder == null) {
            Timber.d("ScannerFragment Tried to start TargetFinder but was not initialized")
            return false
        }
        targetFinder?.startRecognition()
        val objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType())
        if (objectTracker != null) {
            result = objectTracker.start()
        }
        val tManager = TrackerManager.getInstance()
        val deviceTracker =
            tManager.getTracker(PositionalDeviceTracker.getClassType()) as PositionalDeviceTracker?
        if (deviceTracker?.start() == false) {
            Timber.d("ScannerFragment Failed to start device tracker")
            result = false
        } else {
            Timber.d("ScannerFragment Successfully started device tracker")
        }
        return result
    }

    override fun doStopTrackers(): Boolean {
        Timber.d("ScannerFragment doStopTrackers")
        var result = true
        val trackerManager = TrackerManager.getInstance()
        val objectTracker =
            trackerManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker?
        if (objectTracker != null) {
            objectTracker.stop()
            val targetFinder = objectTracker.targetFinder
            targetFinder.stop()
            targetFinder.clearTrackables()
        } else {
            result = false
        }
        return result
    }

    override fun doUnloadTrackersData(): Boolean {
        Timber.d("ScannerFragment doUnloadTrackersData")
        val trackerManager = TrackerManager.getInstance()
        val objectTracker =
            trackerManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker?
        if (objectTracker == null) {
            Timber.d("ScannerFragment Failed to destroy the tracking data set because the ObjectTracker has not been initialized.")
            return false
        }
        return true
    }

    override fun doDeinitTrackers(): Boolean {
        Timber.d("ScannerFragment doDeinitTrackers")
        val trackerManager = TrackerManager.getInstance()
        trackerManager.deinitTracker(ObjectTracker.getClassType())
        return true
    }

    override fun onInitARDone(error: ApplicationException?) {
        if (error != null) {
            vuforiaCallBacks.onInitVuforiaError(error)
            error.printStackTrace()
            return
        }

        Timber.d("ScannerFragment onInitARDone success")

        val glView = ApplicationGLView(context).also { this.glView = it }
        glView.init(true, 16, 0)
        glView.setOnClickListener {
            requestFocus()
        }
        val playbackRenderer: VideoPlaybackRenderer =
            VideoPlaybackRenderer(context, vuforiaAppSession, videoCount).also {
                this.playbackRenderer = it
            }
        playbackRenderer.setTargetRendererListener(this)
        playbackRenderer.setTextures(iconTextures)
        glView.setRenderer(playbackRenderer)

        val temp = floatArrayOf(0f, 0f, 0f)
        for (i in 0 until videoCount) {
            playbackRenderer.setVideoPlayerHelper(i, playerHelperList[i])
            playbackRenderer.targetPositiveDimensions[i].data = temp
            playbackRenderer.videoPlaybackTextureID[i] = -1
            playbackRenderer.requestLoad(i, null, 0, false)
        }
        playbackRenderer.setActive(true)

        val linearLayout = LinearLayout(context)
        linearLayout.orientation = LinearLayout.VERTICAL
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        val frameLayout: FrameLayout = view.findViewById(R.id.cameraView)
        frameLayout.addView(glView, layoutParams)
        vuforiaAppSession.startAR()

        vuforiaInitialized = true
    }

    fun addTarget(vuforiaTarget: VuforiaTarget) {
        val playbackRenderer = this.playbackRenderer ?: return
        playbackRenderer.addTarget(vuforiaTarget)
    }

    private fun requestFocus() {
        CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)
    }

    override fun onVuforiaUpdate(state: State) {
        val finder = targetFinder
        if (finder == null) {
            Timber.d("ScannerFragment Tried to query TargetFinder but was not initialized")
            return
        }
        val queryResult = finder.updateQueryResults()
        val queryStatus = queryResult.status
        if (queryStatus == TargetFinder.UPDATE_RESULTS_AVAILABLE) {
            val queryResultsList = queryResult.results
            if (queryResultsList.empty()) {
                return
            }
            val result = queryResultsList.at(0)
            var cloudRecoResult: CloudRecoSearchResult? = null
            if (result.isOfType(CloudRecoSearchResult.getClassType())) {
                cloudRecoResult = result as CloudRecoSearchResult
            }
            if (cloudRecoResult != null && cloudRecoResult.trackingRating > 0) {
                finder.enableTracking(cloudRecoResult)
            }
        }
    }

    override fun onVuforiaResumed() {
        Timber.d("ScannerFragment onVuforiaResumed")
    }

    override fun onVuforiaStarted() {
        Timber.d("ScannerFragment onVuforiaStarted")
        playbackRenderer?.updateRenderingPrimitives()
        vuforiaCallBacks.setProgressBarVisibility(View.GONE)
        vuforiaCallBacks.onVuforiaStarted()

        if (!firstLaunch) {
            glView?.onPause()
            glView?.onResume()
            firstLaunch = true
        }

        startSaveEnergyTimer()
        vuforiaStarted = true
        setupFocus()
    }

    override fun onSendStatistic(action: StatisticType) {
        val videoPlaybackRenderer: VideoPlaybackRenderer = this.playbackRenderer ?: return
        val vuforiaTarget = videoPlaybackRenderer.currentVideoTarget ?: return
        val videoPlayerHelper = playerHelperList[videoPlaybackRenderer.currentTarget]

        vuforiaCallBacks.sendStatistic(
            action.value,
            videoPlayerHelper.videoTimeInSeconds,
            vuforiaTarget,
            videoPlayerHelper.videoSessionId
        )
    }

    override fun onVideoEnd() {
        val videoPlaybackRenderer: VideoPlaybackRenderer = this.playbackRenderer ?: return
        val currentTargetIndex = videoPlaybackRenderer.currentTarget
        if (currentTargetIndex == -1) {
            return
        }

        val vuforiaTarget = videoPlaybackRenderer.currentVideoTarget ?: return
        val delayMode = vuforiaTarget.vuforiaVideo.delayMode
        val videoPlayerHelper = playerHelperList[videoPlaybackRenderer.currentTarget]

        if (delayMode == 2 || delayMode == 4) {
            videoPlayerHelper.pause()
            vuforiaCallBacks.openLinkUrl(vuforiaTarget)
            onSendStatistic(StatisticType.AUTO_OPEN)
        } else if (delayMode == 3) {
            vuforiaCallBacks.showLinkView(vuforiaTarget)
            videoPlayerHelper.play(0)
        } else {
            videoPlayerHelper.play(0)
        }
    }

    private fun setupFocus() {
        val cameraDevice = CameraDevice.getInstance()
        if (cameraDevice.setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO)) {
            return
        }
        if (cameraDevice.setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)) {
            return
        }
        cameraDevice.setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL)
    }

    fun onConfigurationChanged() {
        vuforiaAppSession.onConfigurationChanged()
    }

    fun clearVuforia() {
        vuforiaInitialized = false

        for (i in 0 until videoCount) {
            playerHelperList[i].deinit()
        }
        try {
            vuforiaAppSession.stopAR()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        iconTextures.clear()
    }

    fun startSaveEnergyTimer() {
        saveEnergyHandler.removeCallbacksAndMessages(null)
        saveEnergyHandler.postDelayed({
            vuforiaCallBacks.onSaveEnergy()
            if (vuforiaStarted)
                pauseVuforia()
        }, SAVE_ENERGY_DELAY)
    }

    private fun clearSaveEnergyDelay() {
        saveEnergyHandler.removeCallbacksAndMessages(null)
    }

    override fun onTargetBlock(vuforiaTarget: VuforiaTarget) {
        vuforiaCallBacks.onTargetLock(vuforiaTarget)
    }

    override fun onRecognizedTarget(
        currentTargetIndex: Int,
        recognizedTarget: VuforiaTarget
    ) {
        val status: VideoPlayerHelper.MEDIA_STATE = playerHelperList[currentTargetIndex].status
        if (status == VideoPlayerHelper.MEDIA_STATE.NOT_READY || status == VideoPlayerHelper.MEDIA_STATE.READY) {
            vuforiaCallBacks.onTargetRecognized(recognizedTarget)

            if (!targetRecognized && recognizedTarget.autoPlay) {
                playVideo(currentTargetIndex)
                Timber.d("onRecognizedTarget %s", currentTargetIndex)
                targetRecognized = true
            }
        } else {
            vuforiaCallBacks.setProgressBarVisibility(View.GONE)
            if (status == VideoPlayerHelper.MEDIA_STATE.PAUSED) {
                resumeVideo(currentTargetIndex)
                vuforiaCallBacks.onPlayVideo()
            }
        }

        //1 pokaz przycisk odrazu i nie otwieraj linku po zakonczeniu filmu
        //pokaz przycisk odrazu i otworz link po zakonczeniu filmu
        //pokaz przycisk po zakonczeniu filmu i nie otwieraj linku po zakonczeniu filmu
        //nie pokazuj przycisku i otworz link po zakonczeniu

        val delayMode = recognizedTarget.vuforiaVideo.delayMode
        when (delayMode) {
            1, 2 -> vuforiaCallBacks.showLinkView(recognizedTarget)
        }

        clearSaveEnergyDelay()
        doStopTracker()
    }

    override fun onNotRecognizedTarget() {
        vuforiaCallBacks.hideLinkView()
        vuforiaCallBacks.onNotRecognizedTarget()
        vuforiaCallBacks.setProgressBarVisibility(View.GONE)
        targetRecognized = false
        doStartTracker()

        startSaveEnergyTimer()
    }

    override fun requestDownloadTarget(imageTarget: ImageTarget) {
        vuforiaCallBacks.requestDownloadTarget(imageTarget)
        startSaveEnergyTimer()
    }

    private fun playVideo(currentTarget: Int) {
        vuforiaCallBacks.onPlayVideo()
        vuforiaCallBacks.setProgressBarVisibility(View.VISIBLE)

        GlobalScope.launch(Dispatchers.Main) {
            val videoPlayerHelper: VideoPlayerHelper = playerHelperList[currentTarget]
            videoPlayerHelper.createMediaPlayer()
        }
    }

    private fun resumeVideo(currentTarget: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            val videoPlayerHelper: VideoPlayerHelper = playerHelperList[currentTarget]
            videoPlayerHelper.resume()
        }
    }

    private fun doStopTracker() {
        val trackerManager = TrackerManager.getInstance()
        val objectTracker =
            trackerManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker
        val targetFinder = objectTracker.targetFinder
        targetFinder.stop()
    }

    private fun doStartTracker() {
        val trackerManager = TrackerManager.getInstance()
        val objectTracker =
            trackerManager.getTracker(ObjectTracker.getClassType()) as ObjectTracker
        val targetFinder = objectTracker.targetFinder
        targetFinder.startRecognition()
    }

    fun onSingleTapConfirmed(e: MotionEvent) {
        if (!vuforiaInitialized || !vuforiaStarted) {
            return
        }

        val videoPlaybackRenderer: VideoPlaybackRenderer = this.playbackRenderer ?: return
        val currentTargetIndex = videoPlaybackRenderer.currentTarget
        if (currentTargetIndex == -1) {
            return
        }

        val tapOnScreenInsideTarget = videoPlaybackRenderer
            .isTapOnScreenInsideTarget(currentTargetIndex, e.x, e.y)
        if (tapOnScreenInsideTarget) {
            val videoPlayerHelper = playerHelperList[currentTargetIndex]
            val videoStatus: VideoPlayerHelper.MEDIA_STATE = videoPlayerHelper.status
            if ((videoStatus == VideoPlayerHelper.MEDIA_STATE.NOT_READY
                        || videoStatus == VideoPlayerHelper.MEDIA_STATE.READY
                        || videoStatus == VideoPlayerHelper.MEDIA_STATE.REACHED_END)
            ) {
                playVideo(currentTargetIndex)
                Timber.d("onSingleTapConfirmed %s", currentTargetIndex)
            }
        }
    }

    fun unlockTarget(vuforiaTarget: VuforiaTarget) {
        val videoPlaybackRenderer: VideoPlaybackRenderer = this.playbackRenderer ?: return
        videoPlaybackRenderer.unlockTarget(vuforiaTarget.id)
    }

    fun isTargetLocked(vuforiaTarget: VuforiaTarget): Boolean {
        val videoPlaybackRenderer: VideoPlaybackRenderer = this.playbackRenderer ?: return false
        return videoPlaybackRenderer.isTargetLock(vuforiaTarget.id)
    }
}