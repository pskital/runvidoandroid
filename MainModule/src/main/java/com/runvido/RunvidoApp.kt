package com.runvido

import com.runvido.dagger.DaggerAppComponent
import com.runvido.dagger.Provider
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class RunvidoApp : DaggerApplication(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder()
            .setApplication(this)
            .build()
        appComponent.inject(this)
        Provider.appComponent = appComponent
        return appComponent
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}