package com.runvido.dagger

import android.app.Application
import com.runvido.RunvidoApp
import com.runvido.dagger.factory.ActivityInjectionFactory
import com.runvido.dagger.factory.FragmentInjectionFactory
import com.runvido.dagger.factory.ViewModelInjectionFactory
import com.runvido.dagger.modules.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        NetworkModule::class,
        ActivityInjectionFactory::class,
        FragmentInjectionFactory::class,
        ViewModelInjectionFactory::class]
)
interface AppComponent : AndroidInjector<RunvidoApp> {

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun setApplication(application: Application): Builder
    }
}