package com.runvido.dagger.factory

import com.runvido.ui.BaseActivity
import com.runvido.ui.scanner.ScannerActivity
import com.runvido.ui.splash.SplashActivity
import com.runvido.ui.tutorial.TutorialActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityInjectionFactory {

    @ContributesAndroidInjector
    abstract fun injectBaseActivity(): BaseActivity

    @ContributesAndroidInjector
    abstract fun injectSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun injectTutorialActivity(): TutorialActivity

    @ContributesAndroidInjector
    abstract fun injectScannerActivity(): ScannerActivity
}