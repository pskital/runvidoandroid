package com.runvido.dagger.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.runvido.dagger.ViewModelKey
import com.runvido.viewmodel.ScannerViewModel
import com.runvido.viewmodel.ViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class ViewModelInjectionFactory {

    @Provides
    fun provideDaggerViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory {
        return viewModelFactory
    }

    @Provides
    @IntoMap
    @ViewModelKey(ScannerViewModel::class)
    fun provideScannerViewModel(viewModel: ScannerViewModel): ViewModel {
        return viewModel
    }
}