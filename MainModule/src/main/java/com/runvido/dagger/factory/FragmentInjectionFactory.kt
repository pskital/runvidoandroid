package com.runvido.dagger.factory

import com.runvido.ui.BaseFragment
import com.runvido.ui.scanner.ScannerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentInjectionFactory {

    @ContributesAndroidInjector
    abstract fun injectBaseFragment(): BaseFragment

    @ContributesAndroidInjector
    abstract fun injectScannerFragment(): ScannerFragment

}