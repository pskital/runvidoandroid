package com.runvido.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Statistic implements Serializable {
    @SerializedName("actionType")
    private String actionType;
    @SerializedName("targetId")
    private String targetId;
    @SerializedName("targetVideoId")
    private String targetVideoId;
    @SerializedName("osVersion")
    private String osVersion;
    @SerializedName("userId")
    private String userId;
    @SerializedName("appVersion")
    private String appVersion;
    @SerializedName("streamingId")
    private String streamingId;
    @SerializedName("timePlayed")
    private Double timePlayed;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("dateTime")
    private String dateTime;

    public Statistic(String actionType, String appVersion, String targetId, String userId,
                     String osVersion, String streamingId, double timePlayed, double latitude, double longitude,
                     String targetVideoId, String dateTime) {
        this.actionType = actionType;
        this.appVersion = appVersion;
        this.targetId = targetId;
        this.userId = userId;
        this.osVersion = osVersion;
        this.streamingId = streamingId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.targetVideoId = targetVideoId;
        this.dateTime = dateTime;

        if (timePlayed == 0) {
            this.timePlayed = null;
        } else {
            this.timePlayed = timePlayed;
        }
    }
}
