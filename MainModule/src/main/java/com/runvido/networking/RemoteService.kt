package com.runvido.networking

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteService {

    @GET("api/core/Mobile/{vuforiaId}")
    fun getTarget(
        @Path("vuforiaId") vuforiaId: String,
        @Query("lat") lat: Double,
        @Query("lng") lng: Double
    ): Single<Response<ResponseBody>>

}