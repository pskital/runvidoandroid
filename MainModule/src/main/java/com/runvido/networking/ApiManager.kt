package com.runvido.networking

import android.app.Application
import com.runvido.R
import com.runvido.utils.hasInternetConnection
import io.reactivex.Single
import io.reactivex.functions.BiPredicate
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ApiManager @Inject constructor(private val context: Application) {

    fun sendRequest(apiRequest: () -> Single<Response<ResponseBody>>): Single<String> {
        return verifyInternetConnection()
            /*
            * do api request
            * */
            .flatMap { request(apiRequest()) }
            /*
            * log exception
            * */
            .doOnError { it.printStackTrace() }
            /*
            * handle exception
            * */
            .onErrorResumeNext { handleExceptions(it) }
            .subscribeOn(Schedulers.io())
    }

    private fun request(request: Single<Response<ResponseBody>>): Single<String> {
        return handleResponseBody(request.retry(retryWhenConnectionTimeout()))
    }

    private fun retryWhenConnectionTimeout(): BiPredicate<Int, Throwable> {
        return BiPredicate { exceptionCount, throwable ->
            Timber.d("ApiManager %s", "exceptionCount:$exceptionCount")
            exceptionCount < 1 && (throwable is SocketTimeoutException
                    || throwable is UnknownHostException)
        }
    }

    private fun verifyInternetConnection(): Single<String> {
        return Single.create {
            Timber.d("ApiManager %s", "verifyInternetConnection")
            if (!context.hasInternetConnection()) {
                it.tryOnError(IOException(context.getString(R.string.no_internet_connection)))
            }
            it.onSuccess("connected")
        }
    }

    private fun handleExceptions(throwable: Throwable): Single<String> {
        return when (throwable) {
            is UnknownHostException -> {
                Single.error(UnknownHostException(context.getString(R.string.un_known_host)))
            }
            is SocketTimeoutException -> {
                Single.error(SocketTimeoutException(context.getString(R.string.timeout_error)))
            }
            else -> {
                Single.error(throwable)
            }
        }
    }

    private fun handleResponseBody(request: Single<Response<ResponseBody>>): Single<String> {
        return request.flatMap { response: Response<ResponseBody> ->
            Single.create<String> {
                val responseBody = response.body()
                val responseString = responseBody?.string()
                if (responseString != null && response.isSuccessful) {
                    it.onSuccess(responseString)
                } else {
                    it.tryOnError(getResponseException(response.errorBody()?.string()))
                }
            }
        }
    }

    private fun getResponseException(response: String?): Exception {
        return Exception(response)
    }
}