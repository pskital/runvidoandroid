package com.runvido.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresPermission
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView

inline fun <T> Context.launchActivity(
    activity: Class<T>,
    init: Intent.() -> Unit = {},
    transitionsBundle: Bundle? = null
) {
    val intent = Intent(this, activity).also(init)
    if (transitionsBundle != null) {
        startActivity(intent, transitionsBundle)
    } else {
        startActivity(intent)
    }
}

fun View.click(click: (View) -> Unit) {
    val clickListener = SingleClickListener {
        click(it)
    }
    setOnClickListener(clickListener)
}

fun AppCompatActivity.replaceFragment(
    fragment: Fragment,
    frameId: Int,
    addToBackStack: Boolean = false,
    clearTop: Boolean = false,
    tag: String = Fragment::class.java.simpleName
) {
    clearTopFragment(supportFragmentManager, clearTop, tag)
    supportFragmentManager.inTransaction({ replace(frameId, fragment, tag) }, addToBackStack, tag)
}

fun AppCompatActivity.addFragment(
    fragment: Fragment,
    frameId: Int,
    addToBackStack: Boolean = false,
    clearTop: Boolean = false,
    tag: String? = null
) {
    clearTopFragment(supportFragmentManager, clearTop, tag)
    supportFragmentManager.inTransaction({ add(frameId, fragment, tag) }, addToBackStack, tag)
}

private fun clearTopFragment(
    supportFragmentManager: FragmentManager?,
    clearTop: Boolean,
    tag: String? = null
) {
    if (clearTop) {
        supportFragmentManager?.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}

fun AppCompatActivity.closeFragment(clearTop: Boolean = false) {
    popBackFragment(supportFragmentManager, clearTop)
}

fun Fragment.closeFragment(clearTop: Boolean = false) {
    popBackFragment(activity?.supportFragmentManager, clearTop)
}

fun Fragment.finish() {
    activity?.finish()
}

inline val Fragment.compactActivity: AppCompatActivity
    get() = requireActivity() as AppCompatActivity

private fun popBackFragment(
    supportFragmentManager: FragmentManager?,
    clearTop: Boolean
) {
    if (clearTop) {
        clearTopFragment(supportFragmentManager, true)
    } else {
        supportFragmentManager?.popBackStack()
    }
}

inline fun FragmentManager.inTransaction(
    func: FragmentTransaction.() -> Unit,
    addToBackStack: Boolean,
    tag: String?
) {

    val fragmentTransaction = beginTransaction()
    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
    fragmentTransaction.func()
    if (addToBackStack) {
        fragmentTransaction.addToBackStack(tag)
    }
    fragmentTransaction.commitAllowingStateLoss()
}

inline val Context.connectivityManager: ConnectivityManager?
    get() = getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager

inline val RecyclerView.ViewHolder.compactActivity: AppCompatActivity
    get() = itemView.context as AppCompatActivity

fun Fragment.hideKeyboard() {
    activity?.hideKeyboard()
}

fun Activity.hideKeyboard() {
    val token = findViewById<View>(android.R.id.content).rootView.windowToken
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(token, 0)
}

fun Activity.getStringExtra(key: String): String? {
    return intent.getStringExtra(key)
}

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun AppCompatActivity.screenWidth(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

@Suppress("DEPRECATION")
@RequiresPermission(value = Manifest.permission.ACCESS_NETWORK_STATE)
fun Context.hasInternetConnection(): Boolean {
    val connectivityManager = this
        .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    connectivityManager?.let { it ->
        val netInfo = it.activeNetworkInfo
        netInfo?.let {
            if (it.isConnected) return true
        }
    }
    return false
}







