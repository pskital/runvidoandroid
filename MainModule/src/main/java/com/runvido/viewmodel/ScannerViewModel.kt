package com.runvido.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.location.Location
import android.location.LocationManager
import com.google.android.gms.location.LocationRequest
import com.google.gson.Gson
import com.microsoft.signalr.HubConnection
import com.microsoft.signalr.HubConnectionBuilder
import com.microsoft.signalr.HubConnectionState
import com.patloew.rxlocation.RxLocation
import com.runvido.BuildConfig
import com.runvido.models.Statistic
import com.runvido.module.models.VuforiaTarget
import com.runvido.module.utils.Constants
import com.runvido.repository.TargetsRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.json.JSONObject
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ScannerViewModel @Inject constructor(
    private val targetsRepository: TargetsRepository,
    private val context: Application
) : BaseViewModel() {

    private val statisticsDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ROOT)

    private var targetDisposable: Disposable? = null
    private var location: Location? = null

    companion object {
        const val MAX_VIDEO_COUNT = 100
    }

    private val rxLocation = RxLocation(context)

    private val locationRequest = LocationRequest.create()
        .setNumUpdates(1)
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        .setInterval(1000)

    private val URL = Constants.STATISTICS_URL + "/statistics/hubs/data"
    //private val URL = "http://localhost:53450/chatHub"

    private val hubConnection: HubConnection =
        HubConnectionBuilder.create(URL)
            .build()

    init {
        startHubConnection()
    }

    @SuppressLint("CheckResult")
    private fun startHubConnection() {
        hubConnection.start().subscribe({
            Timber.d("hub connected")

        }, {

        })
    }

    fun sendStatistic(
        actionType: String,
        timePlayed: Double,
        vuforiaTarget: VuforiaTarget?,
        videoSessionId: String
    ) {

        try {
            val appVersion = BuildConfig.VERSION_NAME
            val targetId: String = vuforiaTarget?.id ?: "933e72e2-c8a4-4c23-9d2b-1d0645217132"
            val userId: String = getUserId()
            val osVersion = "android"

            val latitude = location?.latitude ?: 0.0
            val longitude = location?.longitude ?: 0.0
            val targetVideoId: String = vuforiaTarget?.vuforiaVideo?.id ?: "527a839a-e714-4963-b2cb-c871a78667d3"
            val dateTime: String =
                statisticsDateFormat.format(Date(System.currentTimeMillis()))

            val statistic = Statistic(
                actionType,
                appVersion,
                targetId,
                userId,
                osVersion,
                videoSessionId,
                timePlayed,
                latitude,
                longitude,
                targetVideoId,
                dateTime
            )

            val data = JSONObject(Gson().toJson(statistic)).toString()
            if (hubConnection.connectionState == HubConnectionState.CONNECTED) {
                hubConnection.send("UploadStatistic", statistic)
                Timber.d("hub send $data")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getUserId(): String {
        val deviceIdKey = "device_id_key"
        val sharedPreferences = context.getSharedPreferences("DEVICE_ID", Context.MODE_PRIVATE)
        var deviceId = sharedPreferences.getString(deviceIdKey, null)
        if (deviceId == null) {
            deviceId = UUID.randomUUID().toString()
            sharedPreferences.edit().putString(deviceIdKey, deviceId).apply()
        }
        return deviceId
    }

    fun requestLocation() {
        addDisposable(
            rxLocation.settings().checkAndHandleResolution(locationRequest)
                .flatMap(this::getLocationSingle).subscribe({
                    Timber.d("found location success")
                    this.location = it
                }, {
                    it.printStackTrace()
                })
        )
    }

    fun getTarget(vuforiaId: String, targetsListener: TargetsRepository.TargetListener) {
        if (targetDisposable?.isDisposed == false) {
            return
        }

        val location = getLocation()
        val targetDisposable = targetsRepository.getTarget(vuforiaId, location)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    targetsListener.loadTargetSuccess(it)
                }, {
                    targetsListener.loadTargetError(it)
                }
            )
        this.targetDisposable = targetDisposable
        addDisposable(targetDisposable)
    }

    private fun getLocation(): Location {
        return location ?: Location(LocationManager.NETWORK_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    private fun getLocationSingle(success: Boolean): Single<Location> {
        return if (success) {
            rxLocation.location()
                .updates(locationRequest)
                .first(Location(LocationManager.NETWORK_PROVIDER))
        } else {
            rxLocation.location().lastLocation()
                .toSingle(Location(LocationManager.NETWORK_PROVIDER))
        }
    }

    override fun onCleared() {
        super.onCleared()
        closeHubConnection()
    }

    private fun closeHubConnection() {
        hubConnection.stop()
    }
}