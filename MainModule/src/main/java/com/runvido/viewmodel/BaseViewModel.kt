package com.runvido.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

abstract class BaseViewModel : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun addDisposable(disposable: Disposable, clearDisposables: Boolean = false) {
        if (clearDisposables) {
            clear()
        }
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d("%s", "onCleared() ${javaClass.simpleName}")
        clear()
    }

    open fun clear() {
        compositeDisposable.clear()
    }
}