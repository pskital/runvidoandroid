package com.runvido.repository

import android.app.Application
import android.location.Location
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.runvido.R
import com.runvido.exceptions.VideoTargetException
import com.runvido.module.models.TargetResponse
import com.runvido.module.models.VuforiaTarget
import com.runvido.networking.ApiManager
import com.runvido.networking.RemoteService
import io.reactivex.Single
import retrofit2.Retrofit
import javax.inject.Inject

class TargetsRepository @Inject constructor(
    private val apiManager: ApiManager,
    private val context: Application,
    retrofit: Retrofit
) {

    private val remoteService = retrofit.create(RemoteService::class.java)

    interface TargetListener {

        fun loadTargetSuccess(vuforiaTarget: VuforiaTarget)

        fun loadTargetError(throwable: Throwable)
    }

    fun getTarget(
        vuforiaId: String,
        location: Location
    ): Single<VuforiaTarget> {
        return apiManager.sendRequest {
            remoteService.getTarget(vuforiaId, location.latitude, location.longitude)
        }.map {
            val type = object : TypeToken<TargetResponse>() {}.type
            val data: TargetResponse = Gson().fromJson(it, type)
            val vuforiaTarget = data.vuforiaTarget

            if (vuforiaTarget.vuforiaVideo == null || vuforiaTarget.vuforiaId == null) {
                throw VideoTargetException(context.getString(R.string.video_inactive))
            }

            vuforiaTarget
        }
    }
}