package com.runvido.module.models;

import com.google.gson.annotations.SerializedName;

public class VuforiaVideo {

    @SerializedName("id")
    public String id;

    @SerializedName("convertedFileId")
    public String convertedFileId;

    @SerializedName("buttonText")
    public String buttonText;

    @SerializedName("delayMode")
    public int delayMode;

    @SerializedName("link")
    public String link;

}
