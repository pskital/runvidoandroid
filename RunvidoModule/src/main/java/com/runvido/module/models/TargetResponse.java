package com.runvido.module.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TargetResponse implements Serializable {

    @SerializedName("target")
    public VuforiaTarget vuforiaTarget;

}
