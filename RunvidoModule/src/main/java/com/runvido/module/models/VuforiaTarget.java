package com.runvido.module.models;

import com.google.gson.annotations.SerializedName;

public class VuforiaTarget {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("videoStreamPath")
    public String videoStreamPath;

    @SerializedName("vuforiaId")
    public String vuforiaId;

    @SerializedName("passwordProtection")
    public boolean passwordProtection;

    @SerializedName("password")
    public String password;

    @SerializedName("autoPlay")
    public boolean autoPlay;

    @SerializedName("video")
    public VuforiaVideo vuforiaVideo;
}
