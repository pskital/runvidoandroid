/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is cert trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/


package com.runvido.module.vuforia;
/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is cert trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

import android.annotation.SuppressLint;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.CountDownTimer;
import android.view.Surface;

import com.runvido.module.utils.Constants;
import com.runvido.module.utils.StatisticType;

import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import timber.log.Timber;

import static com.runvido.module.utils.StatisticType.PLAY_VIDEO;

// Helper class for video playback functionality
public class VideoPlayerHelper implements OnPreparedListener,
        OnBufferingUpdateListener, OnCompletionListener, OnErrorListener {
    public static final int CURRENT_POSITION = -1;
    private MediaPlayer mediaPlayer = null;
    private MEDIA_TYPE mVideoType = MEDIA_TYPE.UNKNOWN;
    private SurfaceTexture mSurfaceTexture = null;
    private int mCurrentBufferingPercentage = 0;
    private byte mTextureID = 0;
    private MEDIA_STATE mCurrentState = MEDIA_STATE.NOT_READY;
    private int mSeekPosition = CURRENT_POSITION;
    private ReentrantLock mMediaPlayerLock = null;
    private ReentrantLock mSurfaceTextureLock = null;
    private CountDownTimer countDownTimer;
    private String videoSessionId = "";

    private VideoListener listener;

    public int currentTime = 0;

    public String fileName;

    public void resume() {
        try {
            startTimer(currentTime);
            mediaPlayer.start();
            mCurrentState = MEDIA_STATE.PLAYING;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum MEDIA_STATE {
        REACHED_END(0),
        PAUSED(1),
        STOPPED(2),
        PLAYING(3),
        READY(4),
        NOT_READY(5),
        ERROR(6);

        private final int type;


        MEDIA_STATE(int i) {
            this.type = i;
        }


        public int getNumericType() {
            return type;
        }
    }

    public enum MEDIA_TYPE {
        ON_TEXTURE(0),
        FULLSCREEN(1),
        ON_TEXTURE_FULLSCREEN(2),
        UNKNOWN(3);

        private final int type;


        MEDIA_TYPE(int i) {
            this.type = i;
        }

        @SuppressWarnings("unused")
        public int getNumericType() {
            return type;
        }
    }

    public void init() {
        mMediaPlayerLock = new ReentrantLock();
        mSurfaceTextureLock = new ReentrantLock();

    }

    public void deinit() {
        unload();

        mSurfaceTextureLock.lock();
        mSurfaceTexture = null;
        mSurfaceTextureLock.unlock();

    }

    @SuppressWarnings("UnusedReturnValue")
    @SuppressLint("NewApi")
    public boolean load(String filename, MEDIA_TYPE requestedType, int seekPosition) {
        fileName = filename;
        boolean canBeOnTexture = false;
        boolean canBeFullscreen = false;

        boolean result = false;
        mMediaPlayerLock.lock();
        mSurfaceTextureLock.lock();

        if ((mCurrentState == MEDIA_STATE.READY) || (mediaPlayer != null)) {
        } else {
            if (((requestedType == MEDIA_TYPE.ON_TEXTURE) || // If the client requests on texture only
                    (requestedType == MEDIA_TYPE.ON_TEXTURE_FULLSCREEN))) // or on texture with full screen
            {
                if (mSurfaceTexture == null) {

                } else {
                    canBeOnTexture = true;
                }
            }

            mSeekPosition = seekPosition;

            if (canBeFullscreen && canBeOnTexture)
                mVideoType = MEDIA_TYPE.ON_TEXTURE_FULLSCREEN;
            else if (canBeFullscreen) {
                mVideoType = MEDIA_TYPE.FULLSCREEN;
                mCurrentState = MEDIA_STATE.READY;
            } else if (canBeOnTexture)
                mVideoType = MEDIA_TYPE.ON_TEXTURE;
            else
                mVideoType = MEDIA_TYPE.UNKNOWN;

            result = true;
        }

        mSurfaceTextureLock.unlock();
        mMediaPlayerLock.unlock();

        return result;
    }

    public void createMediaPlayer() {
        try {
            if (mCurrentState == MEDIA_STATE.PAUSED) {
                play(getCurrentPosition());
            } else {
                if (mediaPlayer != null) {
                    try {
                        mediaPlayer.stop();
                    } catch (Exception e) {
                        mMediaPlayerLock.unlock();
                    }

                    mediaPlayer.release();
                    mediaPlayer = null;
                }

                mediaPlayer = new MediaPlayer();
                Timber.d("VideoPlayerHelper fileName: %s", fileName);
                String videoUrl = Constants.API_URL + fileName;

                mediaPlayer.setDataSource(videoUrl);
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnCompletionListener(this);
                mediaPlayer.setOnBufferingUpdateListener(this);
                mediaPlayer.setOnErrorListener(this);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                Surface surface = new Surface(mSurfaceTexture);
                mediaPlayer.setSurface(surface);
                mediaPlayer.prepareAsync();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mCurrentState = MEDIA_STATE.NOT_READY;

            if (mMediaPlayerLock.isHeldByCurrentThread()) {
                mMediaPlayerLock.unlock();
            }

            if (mSurfaceTextureLock.isHeldByCurrentThread()) {
                mSurfaceTextureLock.unlock();
            }
        }
    }

    public void unload() {
        mMediaPlayerLock.lock();
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
            } catch (Exception e) {
                mMediaPlayerLock.unlock();
            }

            mediaPlayer.release();
            mediaPlayer = null;
        }
        mMediaPlayerLock.unlock();

        mCurrentState = MEDIA_STATE.NOT_READY;
    }

    public boolean isPlayableOnTexture() {
        return (mVideoType == MEDIA_TYPE.ON_TEXTURE)
                || (mVideoType == MEDIA_TYPE.ON_TEXTURE_FULLSCREEN);

    }

    public MEDIA_STATE getStatus() {
        return mCurrentState;
    }

    public int getVideoWidth() {
        if (!isPlayableOnTexture()) {
            return -1;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)
                || (mCurrentState == MEDIA_STATE.REACHED_END)) {
            return -1;
        }

        int result = -1;
        mMediaPlayerLock.lock();
        if (mediaPlayer != null)
            try {
                result = mediaPlayer.getVideoWidth();
            } catch (Exception e) {
                e.printStackTrace();
            }
        mMediaPlayerLock.unlock();

        return result;
    }

    public int getVideoHeight() {
        if (!isPlayableOnTexture()) {
            return -1;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)
                || (mCurrentState == MEDIA_STATE.REACHED_END)) {
            return -1;
        }

        int result = -1;
        mMediaPlayerLock.lock();
        if (mediaPlayer != null)
            try {
                result = mediaPlayer.getVideoHeight();
            } catch (Exception e) {
                e.printStackTrace();
            }
        mMediaPlayerLock.unlock();

        return result;
    }

    @SuppressWarnings("unused")
    public float getLength() {
        if (!isPlayableOnTexture()) {
            return -1;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return -1;
        }

        int result = -1;
        mMediaPlayerLock.lock();
        if (mediaPlayer != null)
            result = mediaPlayer.getDuration() / 1000;
        mMediaPlayerLock.unlock();

        return result;
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean play(int seekPosition) {
        if (seekPosition == 0 || seekPosition == -1) {
            startTimer(mediaPlayer.getDuration());
            currentTime = 0;
            generateVideoSessionId();
        }

        if (!isPlayableOnTexture()) {
            return false;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return false;
        }

        mMediaPlayerLock.lock();
        if (seekPosition != CURRENT_POSITION) {
            try {
                mediaPlayer.seekTo(seekPosition);
            } catch (Exception e) {
                mMediaPlayerLock.unlock();
            }
        } else {
            if (mCurrentState == MEDIA_STATE.REACHED_END) {
                try {
                    mediaPlayer.seekTo(0);
                } catch (Exception e) {
                    mMediaPlayerLock.unlock();
                }
            }
        }

        try {
            mediaPlayer.start();
        } catch (Exception e) {
            mMediaPlayerLock.unlock();
        }
        mCurrentState = MEDIA_STATE.PLAYING;

        mMediaPlayerLock.unlock();
        Timber.d("VideoPlayerHelper play");
        return true;
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean pause() {
        if (!isPlayableOnTexture()) {
            return false;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return false;
        }

        boolean result = false;

        mMediaPlayerLock.lock();
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                try {
                    mediaPlayer.pause();
                    countDownTimer.cancel();
                    //currentTime = mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition();
                } catch (Exception e) {
                    e.printStackTrace();
                    mMediaPlayerLock.unlock();
                }
                mCurrentState = MEDIA_STATE.PAUSED;
                result = true;
            }
        }
        mMediaPlayerLock.unlock();

        return result;
    }

    public void pauseDefinitly() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            try {
                mediaPlayer.pause();
            } catch (Exception e) {
                mMediaPlayerLock.unlock();
                e.printStackTrace();
            }
            mCurrentState = MEDIA_STATE.PAUSED;
        }

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    // Stops the current movie being played
    @SuppressWarnings("unused")
    public boolean stop() {
        if (!isPlayableOnTexture()) {
            return false;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return false;
        }

        boolean result = false;

        mMediaPlayerLock.lock();
        if (mediaPlayer != null) {
            mCurrentState = MEDIA_STATE.STOPPED;
            try {
                mediaPlayer.stop();
            } catch (Exception e) {
                mMediaPlayerLock.unlock();
                e.printStackTrace();
            }

            result = true;
        }
        mMediaPlayerLock.unlock();
        mSurfaceTexture = null;
        return result;
    }

    @SuppressWarnings("UnusedReturnValue")
    @SuppressLint("NewApi")
    public byte updateVideoData() {
        if (!isPlayableOnTexture()) {
            // Log.d( LOGTAG,
            return -1;
        }

        byte result = -1;

        mSurfaceTextureLock.lock();
        if (mSurfaceTexture != null) {
            // Only request an update if currently playing
            if (mCurrentState == MEDIA_STATE.PLAYING) {
                try {
                    mSurfaceTexture.updateTexImage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            result = mTextureID;
        }
        mSurfaceTextureLock.unlock();
        return result;
    }

    @SuppressWarnings("unused")
    public boolean seekTo(int position) {
        if (!isPlayableOnTexture()) {
            return false;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return false;
        }

        boolean result = false;
        mMediaPlayerLock.lock();
        if (mediaPlayer != null) {
            try {
                mediaPlayer.seekTo(position);
            } catch (Exception e) {
                mMediaPlayerLock.unlock();
                e.printStackTrace();
            }
            result = true;
        }
        mMediaPlayerLock.unlock();

        return result;
    }

    public int getCurrentPosition() {
        if (!isPlayableOnTexture()) {
            return -1;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return -1;
        }

        int result = -1;
        mMediaPlayerLock.lock();
        if (mediaPlayer != null)
            result = mediaPlayer.getCurrentPosition();
        mMediaPlayerLock.unlock();

        return result;
    }

    @SuppressWarnings("unused")
    public boolean setVolume(float value) {
        if (!isPlayableOnTexture()) {
            return false;
        }

        if ((mCurrentState == MEDIA_STATE.NOT_READY)
                || (mCurrentState == MEDIA_STATE.ERROR)) {
            return false;
        }

        boolean result = false;
        mMediaPlayerLock.lock();
        if (mediaPlayer != null) {
            mediaPlayer.setVolume(value, value);
            result = true;
        }
        mMediaPlayerLock.unlock();

        return result;
    }

    @SuppressWarnings("unused")
    public int getCurrentBufferingPercentage() {
        return mCurrentBufferingPercentage;
    }


    // Listener call for buffering
    public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
        mMediaPlayerLock.lock();
        if (mediaPlayer != null) {
            if (arg0 == mediaPlayer) {
                mCurrentBufferingPercentage = arg1;
                //listener.onVideoUpdate(arg1);
            }
        }
        mMediaPlayerLock.unlock();
    }

    public void onCompletion(MediaPlayer arg0) {
        mCurrentState = MEDIA_STATE.REACHED_END;
        listener.onVideoEnd();
    }

    @SuppressLint("NewApi")
    void setupSurfaceTexture(int TextureID) {
        if (mSurfaceTexture == null) {
            mSurfaceTextureLock.lock();
            mSurfaceTexture = new SurfaceTexture(TextureID);
            mTextureID = (byte) TextureID;
            mSurfaceTextureLock.unlock();
        }
    }


    @SuppressLint("NewApi")
    public void getSurfaceTextureTransformMatrix(float[] mtx) {
        mSurfaceTextureLock.lock();
        if (mSurfaceTexture != null)
            mSurfaceTexture.getTransformMatrix(mtx);
        mSurfaceTextureLock.unlock();
    }


    public void onPrepared(MediaPlayer mediaplayer) {
        Timber.d("VideoPlayerHelper onPrepared");

        mCurrentState = MEDIA_STATE.READY;

        mSeekPosition = 0;
        play(mSeekPosition);
    }

    public void setListener(VideoListener listener) {
        this.listener = listener;
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Timber.d("VideoPlayerHelper onError %s", what);

        if (mp == mediaPlayer) {
            switch (what) {
                case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                    break;
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    break;
                case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                    break;
                default:
            }
            unload();
            mCurrentState = MEDIA_STATE.ERROR;
            return true;
        }
        return false;
    }

    private void startTimer(long time) {
        final int amoungToupdate = 1000;
        countDownTimer = new CountDownTimer(time, amoungToupdate) {
            @Override
            public void onTick(long l) {
                currentTime += amoungToupdate;
                listener.onSendStatistic(PLAY_VIDEO);
            }

            @Override
            public void onFinish() {
                currentTime += amoungToupdate;
                if (mediaPlayer != null) {
                    listener.onSendStatistic(PLAY_VIDEO);
                }
            }
        };
        countDownTimer.start();
    }

    @Deprecated
    @SuppressWarnings("unused")
    public String getVideoTime() {
        int time = currentTime / 1000;
        long hours = time / 3600;
        long minutes = (time % 3600) / 60;
        long seconds = time % 60;
        return String.format(Locale.ROOT, "%02d:%02d:%02d", hours, minutes, seconds);
    }

    private void generateVideoSessionId() {
        String uuui = UUID.randomUUID().toString();
        this.videoSessionId = uuui;
    }

    public String getVideoSessionId() {
        return videoSessionId;
    }

    public double getVideoTimeInSeconds() {
        return currentTime / 1000.0;
    }

    public interface VideoListener {

        void onSendStatistic(StatisticType action);

        void onVideoEnd();
    }
}
