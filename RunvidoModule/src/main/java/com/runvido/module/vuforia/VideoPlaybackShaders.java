/*===============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.runvido.module.vuforia;

class VideoPlaybackShaders {

    public static final String VIDEO_PLAYBACK_VERTEX_SHADER = " \n"
            + "attribute vec4 vertexPosition; \n"
            + "attribute vec2 vertexTexCoord; \n"
            + "varying vec2 texCoord; \n"
            + "uniform mat4 modelViewProjectionMatrix; \n"
            + "\n"
            + "void main() \n"
            + "{ \n"
            + "   gl_Position = modelViewProjectionMatrix * vertexPosition; \n"
            + "   texCoord = vertexTexCoord; \n"
            + "} \n";

    public static final String VIDEO_PLAYBACK_FRAGMENT_SHADER = " \n"
            + "#extension GL_OES_EGL_image_external : require \n"
            + "precision mediump float; \n"
            + "varying vec2 texCoord; \n"
            + "uniform samplerExternalOES texSamplerOES; \n" + " \n"
            + "void main() \n"
            + "{ \n"
            + "   gl_FragColor = texture2D(texSamplerOES, texCoord); \n"
            + "} \n";
}