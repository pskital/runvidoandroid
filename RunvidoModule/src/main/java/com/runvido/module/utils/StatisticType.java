package com.runvido.module.utils;

public enum StatisticType {
    PLAY_VIDEO("play_video"),
    BUTTON_CLICK("button_click"),
    AUTO_OPEN("link_autoopen");

    public String value;

    StatisticType(String type) {
        value = type;
    }
}
